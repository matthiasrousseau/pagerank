# -*- coding: utf-8 -*-

#Cette class lit un file en entrée et retourne une liste d'adjacence.

class GraphMaker:
    def __init__(self):
        self.graphe = None
        self.nbElements = 0

    def toGraph(self, graphe):
        if graphe is None:
            raise ValueError("No text has been set")
        
        # On ne regénère pas le graphe si on en a pas chargé un nouveau.
        if self.graphe:
            return (self.nbElements, self.graphe)

        

        if type(graphe) is str:
            lines = graphe.split("\n")
            self.nbElements = int(lines[0])
            lines = lines[1:]
        else: #file
            lines = graphe
            self.nbElements = int(lines.readline())
        print("Lecture du fichier")

        self.graphe =  [[] for x in xrange(self.nbElements+1)]
        
        numbersLinks = [0] * (self.nbElements) # nombre de pages vers laquelle la page pointe

        for line in lines:
            values = line.split(" -> ")
            depart = int(values[0])
            arrivee = int(values[1])
            self.graphe[arrivee].append(depart)
            numbersLinks[depart-1] = numbersLinks[depart-1] + 1
        
        print("Fin lecture de fichier")
            
        return (numbersLinks, self.graphe)

    def outGraph(self, resultat, out):

        out_file = open(out, "w")
        if resultat is None:
            result = self.result
        
        for k, v in enumerate(resultat):
            if(k > 0):
                out_file.write("PageRank de %d = %.2f%% \n"%(k, v*100))
        
        out_file.close()
