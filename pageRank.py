#!/usr/bin/python
# -*- coding: utf-8 -*-
import argparse
from optparse import OptionParser
import utils
import os
import sys
import numpy
import datetime

class PageRank:
    def __init__(self, graphe, d = 0.85):
        self.graphe = graphe[1]
        self.result = None
        self.elementsPointes = graphe[0]
        self.elementsPointentPersonne = [(key+1) for (key,i) in enumerate(self.elementsPointes) if i == 0]
        self.n = len(graphe[0])                     # nombre de sommets
        self.D = d                                  # Probabilité qu'à l'itération courante,l'internaute continue de cliquer sur un lien de la page courante.
        self.constante = ((1-self.D) /self.n)       # Probabilité que l'internaute choisisse au hasard l'une des 1/n pages.
        self.rangeAll = set(range(1, self.n+1))

    def printResult(self, result = None):
        if result is None:
            result = self.result
        
        for k, v in enumerate(resultat):
            if(k > 0):
                print("PageRank de %d = %.2f%%"%(k, v*100))

    def getResult(self):
        #Le résultat a été calculé pour le graphe actuel, on ne le recalcul pas.
        if self.result:
            return self.result

        iterations = 1
        
        result = self._stepOne()
        prev = result
        
        #print("Itération %d"%(iterations))
        result = self._step(result)

        while not numpy.allclose(result,prev, rtol=1e-02):
            iterations = iterations + 1
            #print("Itération %d"%(iterations))
            prev = result
            result = self._step(result)

        print str(iterations) + " itérations"

        self.result = result

        return self.result

    def _stepOne(self):
        """ Initialise le dict de résultat """

        firstValue = 1/float(self.n)
        result = [firstValue for x in self.rangeAll]
        result.append(firstValue)
        
        return result

    def _step(self, listResult):
        listResult2 = listResult[:]

        elementsPointes = self.elementsPointes
        graphe = self.graphe

        # On parcourt le dictionnaire des résultats. 
        for k in self.rangeAll:
            sommes = 0
            # On regarde si des pages pointent vers k.
            for k2 in graphe[k]: # elements qui pointent vers k
                nbArcs = elementsPointes[k2-1]
                scoreK = listResult[k2]
                sommes += (scoreK/nbArcs)

            for k2 in self.elementsPointentPersonne:
                scoreK = listResult[k2]
                sommes += (scoreK/self.n)
            
            listResult2[k] = self.constante + (self.D * sommes)
            
        return listResult2

    

if __name__ == "__main__":
    # Timer.
    now = datetime.datetime.now()

    # Parsing des arguments
    parser = argparse.ArgumentParser(description="Calcul de pageRank d'un graphe")
    parser.add_argument("graphe", type=str, help="graphe en entrée",nargs='?')
    parser.add_argument("-t", "--txt", dest="out", help="sortie dans un format text")
    args = parser.parse_args()
   
    
    # Chargement du graphe
    if args.graphe is None:
        graphe = ""
        graphe = sys.stdin
    else:
        graphe = file(args.graphe, 'r')
 
    graphe = utils.GraphMaker().toGraph(graphe)
    
    # Traitement du graphe
    pageRank = PageRank(graphe)
    resultat = pageRank.getResult()

    # Affichage du résultat.
    if args.out is None:
        pageRank.printResult(resultat)
    else:
        utils.GraphMaker().outGraph(resultat, args.out)

    print "Temps d'exécution : {}".format(datetime.datetime.now() - now)
